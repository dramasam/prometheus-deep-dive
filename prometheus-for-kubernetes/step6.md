# Configure Direct mode
Export IpPool config.

`calicoctl get ippool default-ipv4-ippool -o yaml > ippool.yaml`{{execute}}

Disable IpPool and apply yaml.

`calicoctl apply -f ippool.yaml`{{execute}}