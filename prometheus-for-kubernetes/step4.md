## Install calicoctl

Log into the host, open a terminal prompt, and navigate to the location where you want to install the binary.

`cd /usr/local/bin/`{{execute}}

Use the following command to download the calicoctl binary.

`curl -O -L  https://github.com/projectcalico/calicoctl/releases/download/v3.16.3/calicoctl`{{execute}}

Set the file to be executable.

`chmod +x calicoctl`{{execute}}

Set datastote as 'kubernetes'

`export DATASTORE_TYPE=kubernetes`{{execute}}

Set Kubernetes config path

`export KUBECONFIG=~/.kube/config`{{execute}}

Check enpoints,

`calicoctl get workloadendpoints`{{execute}}

Get BGP peer status.

`calicoctl node status`{{execute}}

Get IPAM status.

`calicoctl ipam show`{{execute}}
