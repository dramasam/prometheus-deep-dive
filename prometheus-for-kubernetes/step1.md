## Create Kubernetes Cluster

Let's setup Kubernetes Cluster by executing the following command,

`kubeadm init`{{execute}}

Create path for default kubeconfig.

`mkdir -p $HOME/.kube`{{execute}}

Copy the KubeConfig to default path.
`sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config`{{execute}}

Change the owner.
`sudo chown $(id -u):$(id -g) $HOME/.kube/config`{{execute}}

## Add worker node to the Cluster

Copy the 'kubeadmn join' from the cluster init result and execute in worker node terminal; Check the node list in master node.

`kubectl get nodes`{{execute}}

Check POD status.

`kubectl get pods --all-namespaces`{{execute}}