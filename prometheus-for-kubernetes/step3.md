## Install Calico

Download calico.yaml

`curl https://docs.projectcalico.org/manifests/calico.yaml -O`{{execute}}


Create calico daemonset.

`kubectl apply -f calico.yaml`{{execute}}

## Check node and Pod staus

Copy the 'kubeadmn join' from the cluster init result and execute in worker node terminal; Check the node list in master node.

`kubectl get nodes`{{execute}}

Check POD status.

`kubectl get pods --all-namespaces`{{execute}}